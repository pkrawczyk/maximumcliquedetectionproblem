Java program, which implements Bron-Kerbosch algorithm for maximal clique detection. The whole source code is in one file, because it was an exercise requirement. 

It contains some additional conditions from the exercise:

 If the super groups (clique) have equal sizes, program verifies the number of friends, which do not belong to the given
 super group (condition 2). If there are more than one group, which fits into those two  conditions, the result will be the super group, which sorted list of names is smallest lexicographically (condition 3).