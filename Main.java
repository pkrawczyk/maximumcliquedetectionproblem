import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

/**
 * Program for finding the biggest super group among the list of friends (condition 1). If the super
 * groups have equal sizes, it verifies the number of friends, which do not belong to the given
 * super group (condition 2). If there are more than one group, which fits into those two
 * conditions, the result will be the super group, which sorted list of names is smallest
 * lexicographically (condition 3).
 * 
 * The problem is solved by using maximal clique detection algorithm @see <a
 * href="https://en.wikipedia.org/wiki/Clique_problem">See more:
 * https://en.wikipedia.org/wiki/Clique_problem</a>. Program implements Bron-Kerbosch algorithm @see
 * <a href="https://en.wikipedia.org/wiki/Bron%E2%80%93Kerbosch_algorithm">https://en.wikipedia.org/
 * wiki/Bron%E2%80%93Kerbosch_algorithm</a>
 * 
 * 
 * SIDENOTES: Generally code should be splitted into few classes for readability, but the exercise
 * specifies that everything should be in one file.
 * 
 * @author Pawel Krawczyk
 */
public class Main
{
	/** GLOBAL VARIABLES */
	private static FriendsList sFriendsList;
	// Using TreeSet to sort elements in the group/clique
	private static Collection<TreeSet<String>> sSuperGroups;

	/**
	 * Program start point.
	 */
	public static void main(String[] args) throws IOException
	{
		// sFriendsList = getMockFriendList(); // Used in debug phase
		sFriendsList = validateInputDataAndReturnFriendsList(); // Used for final version
		if (sFriendsList == null)
		{
			return;
		}
		Collection<TreeSet<String>> biggestSuperGroups = getBiggestSuperGroups();
		if (biggestSuperGroups.isEmpty())
		{
			System.out.println("There are no super groups. Please try with another input data.");
			return;
		}
		// If size of found super groups is bigger than 1, second conditions need to be checked.
		if (biggestSuperGroups.size() > 1)
		{
			int biggestSuperGroupSize = biggestSuperGroups.iterator().next().size();
			Collection<TreeSet<String>> afterSecondCondition = getGroupsWithBiggestNumberOfFriendsOutside(biggestSuperGroups);
			// If size of found super groups is bigger than 1, third condition needs to be checked
			if (afterSecondCondition.size() > 1)
			{
				String nameList = findSmallestLexGroup(afterSecondCondition);
				printWinner(biggestSuperGroupSize, nameList);
			}
			// There is only one super group with the maximum size and maximum number of friends
			// outside the group, so we can print winner
			else
			{
				Set<String> winnerSet = afterSecondCondition.iterator().next();
				printWinner(winnerSet.size(), winnerSet.toString());
			}
		}
		// There is only one super group with the maximum size, so we can print winner
		else
		{
			Set<String> winnerSet = biggestSuperGroups.iterator().next();
			printWinner(winnerSet.size(), winnerSet.toString());
		}
	}

	/** METHODS CONNECTED WITH THE ALGORITHM */

	/**
	 * Finds super group using Bron-Kerbosch algorithm @see <a
	 * href="https://en.wikipedia.org/wiki/Bron%E2%80%93Kerbosch_algorithm"
	 * >https://en.wikipedia.org/ wiki/Bron%E2%80%93Kerbosch_algorithm</a>
	 */
	private static void findSuperGroups(List<String> potentialSuperGroup,
	        List<String> candidatesForSuperGroup, List<String> foundSuperGroups)
	{
		List<String> candidates = new ArrayList<String>(candidatesForSuperGroup);
		if (!canFinish(candidatesForSuperGroup, foundSuperGroups))
		{
			for (String candidate : candidates)
			{
				List<String> newFound = new ArrayList<String>();
				List<String> newCandidates = new ArrayList<String>();

				potentialSuperGroup.add(candidate);
				candidatesForSuperGroup.remove(candidate);

				for (String newCandidate : candidatesForSuperGroup)
				{
					if (sFriendsList.isPair(candidate, newCandidate))
					{
						newCandidates.add(newCandidate);
					}
				}
				for (String foundItem : foundSuperGroups)
				{
					if (sFriendsList.isPair(candidate, foundItem))
					{
						newFound.add(foundItem);
					}
				}
				if (newCandidates.isEmpty() && newFound.isEmpty())
				{
					// is biggest super group
					sSuperGroups.add(new TreeSet<String>(potentialSuperGroup));
				}
				else
				{
					// recursive invocation
					findSuperGroups(potentialSuperGroup, newCandidates, newFound);
				}
				foundSuperGroups.add(candidate);
				potentialSuperGroup.remove(candidate);
			}
		}
	}

	/**
	 * Checks if there are more iterations to do
	 */
	private static boolean canFinish(List<String> possibleSuperGroup, List<String> foundSuperGroup)
	{
		int peopleCount;
		boolean canFinish = false;
		for (String found : foundSuperGroup)
		{
			peopleCount = 0;
			for (String superGroup : possibleSuperGroup)
			{
				if (sFriendsList.isPair(found, superGroup))
				{
					peopleCount++;
				}
			}
			if (peopleCount == possibleSuperGroup.size())
			{
				canFinish = true;
			}
		}
		return canFinish;
	}

	/**
	 * Finds the biggest super groups.
	 *
	 * @return Collection of super groups
	 */
	private static Collection<TreeSet<String>> getBiggestSuperGroups()
	{
		int maximum = 0;
		getAllSuperGroups();
		Collection<TreeSet<String>> biggestSuperGroups = new ArrayList<TreeSet<String>>();
		// Finding size of biggest super group
		for (TreeSet<String> superGroup : sSuperGroups)
		{
			if (maximum < superGroup.size())
			{
				maximum = superGroup.size();
			}
		}
		// Adding to the biggestSuperGroups collection, only group with size equal to the maximum
		for (TreeSet<String> superGroup : sSuperGroups)
		{
			if (maximum == superGroup.size())
			{
				biggestSuperGroups.add(superGroup);
			}
		}
		return biggestSuperGroups;
	}

	/**
	 * Finds all of the super groups
	 * 
	 * @return Collection of the super groups
	 */
	private static Collection<TreeSet<String>> getAllSuperGroups()
	{
		// Initializing the lists outside of the #findSuperGroups method, because it uses reflection
		List<String> potentialSuperGroup = new ArrayList<String>();
		List<String> candidatesForSuperGroup = new ArrayList<String>();
		List<String> foundSuperGroup = new ArrayList<String>();
		// Adding input data
		candidatesForSuperGroup.addAll(sFriendsList.getNames());
		sSuperGroups = new ArrayList<TreeSet<String>>();
		findSuperGroups(potentialSuperGroup, candidatesForSuperGroup, foundSuperGroup);
		return sSuperGroups;
	}

	/** METHODS FOR CHECKING CONDITIONS 2 AND 3 */

	/**
	 * Method for checking which super group has the biggest number of friends outside the group.
	 * Checks the second condition.
	 * 
	 * @param biggestSuperGroups
	 *            Collection of the super groups, after checking first condition.
	 * @return Collection of the super groups, after checking second condition.
	 */
	private static Collection<TreeSet<String>> getGroupsWithBiggestNumberOfFriendsOutside(
	        Collection<TreeSet<String>> biggestSuperGroups)
	{
		Collection<TreeSet<String>> groupsWithBiggestNumberOfFriendsOutside = new ArrayList<TreeSet<String>>();
		// Map for keeping the super group and corresponding number of friends outside the group
		Map<TreeSet<String>, Integer> groupsAndFriendsOutsideTheGroupCountMap = new HashMap<TreeSet<String>, Integer>();
		int maximumResult = -1;
		for (TreeSet<String> superGroup : biggestSuperGroups)
		{
			int result = getFriendsOutsideGroup(superGroup);
			if (result > maximumResult)
			{
				maximumResult = result;
			}
			groupsAndFriendsOutsideTheGroupCountMap.put(superGroup, result);
		}
		// Coping super groups with the biggest number of friends outside the group
		// Must be done, because the maximumResult can grow while iterating the biggestSuperGroups,
		// therefore map can contain sets with lower number of friends outside the group
		for (Map.Entry<TreeSet<String>, Integer> entry : groupsAndFriendsOutsideTheGroupCountMap
		        .entrySet())
		{
			if (entry.getValue() == maximumResult)
			{
				groupsWithBiggestNumberOfFriendsOutside.add(entry.getKey());
			}
		}
		return groupsWithBiggestNumberOfFriendsOutside;
	}

	/**
	 * Method for calculating the sum of friends outside the super group.
	 * 
	 * @param friendsGroup
	 *            Super group of friends.
	 * @return Summarized number of friends outside the super group.
	 */
	private static int getFriendsOutsideGroup(Set<String> friendsGroup)
	{
		int cliqueSize = friendsGroup.size();
		int finalSum = 0;
		for (String name : friendsGroup)
		{
			//
			int friendsOutsideGroup = sFriendsList.getFriendsCount().get(name) - cliqueSize + 1;
			finalSum = finalSum + friendsOutsideGroup;
			// System.out.println(name + " has " + friendsOutsideGroup + " other friends"); //
			// debug log
		}
		// System.out.println("Final sum: " + finalSum); // debug log
		return finalSum;
	}

	/**
	 * Finds the list of names of the smallest lexicographically super group. Third Condition
	 * checker.
	 * 
	 * @param afterSecondCondition
	 *            Sets of the super groups candidates.
	 * @return String with list of names with the smallest lexicographically order.
	 */
	private static String findSmallestLexGroup(Collection<TreeSet<String>> afterSecondCondition)
	{
		List<String> nameList = new ArrayList<String>();
		for (TreeSet<String> superGroup : afterSecondCondition)
		{
			nameList.add(superGroup.toString());
		}
		Collections.sort(nameList);
		if (nameList.size() > 0)
		{
			return nameList.get(0);
		}
		return "No list";
	}

	/** METHODS FOR VALIDATING AND READING INPUT DATA AND PRINTING MESSAGES */

	private static FriendsList validateInputDataAndReturnFriendsList()
	{
		FriendsList friendsList = null;
		Scanner scanner = null;
		boolean needMoreData = true;
		try
		{
			scanner = new Scanner(System.in);
			while (needMoreData)
			{
				System.out
				        .print("Please enter input data(two integers: N-number of unique names[2-18], M-number of friend pairs[1-100], then M lines with names pair):\n");
				friendsList = new Main().new FriendsList();
				int n, m = -1;
				try
				{
					n = scanner.nextInt();
					if (n < 2 || n > 18)
					{
						System.out.println("N should be value between 2 and 18!");
						continue;
					}
					m = scanner.nextInt();
					if (m < 1 || m > 100)
					{
						System.out.println("M should be value between 1 and 100!");
						continue;
					}
				}
				catch (InputMismatchException exception)
				{
					System.out.println("Given parameter is not an integer");
					continue;
				}
				readNameList(friendsList, scanner, m);
				// Validate if the number of inputed names is correct
				if (friendsList.getNames().size() != n)
				{
					System.out
					        .println("The number of given names does not match the N value specified. Try again");
					continue;
				}
				needMoreData = false;
			}
		}
		finally
		{
			if (scanner != null)
			{
				scanner.close();
			}
		}
		return friendsList;
	}

	private static int readNameList(FriendsList friendsList, Scanner scanner, int m)
	{
		int pairsToAdd = m;
		while (pairsToAdd > 0)
		{
			String line = scanner.nextLine();
			String[] splited = line.split("\\s+");
			if (splited == null || splited.length == 0)
			{
				continue;
			}
			int inputedNames = splited.length;
			switch (inputedNames)
			{
			case 1:
				continue;
			case 2:
				String first = splited[0];
				String second = splited[1];
				if (first.equals(second))
				{
					System.out.println("Names cannot be the same");
					continue;
				}
				if (validateNamesLenght(first, second) && validateLetters(first, second))
				{
					if (friendsList.addPair(first, second))
					{
						friendsList.addName(first);
						friendsList.addName(second);
						pairsToAdd--;
					}
					else
					{
						System.out.println("Given pair was already added");
					}
				}
				else
				{
					System.out
					        .println("Some names are too long or contains invalid characters. Use only English Alphabet (a-zA-Z), up to 16 characters");
				}
				break;
			case 3:
				System.out
				        .println("Too many parameters. Please input only two parameters separated by space");
				break;

			default:
				break;
			}
		}
		return pairsToAdd;
	}

	private static boolean validateLetters(String... names)
	{
		for (String name : names)
		{
			for (char c : name.toCharArray())
			{
				if (!(isLatinLetter(c)))
				{
					return false;
				}
			}
		}
		return true;
	}

	private static boolean isLatinLetter(char c)
	{
		return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z');
	}

	private static boolean validateNamesLenght(String... names)
	{
		for (String name : names)
		{
			if (name.length() > 16)
			{
				return false;
			}
		}
		return true;
	}

	/**
	 * Method for printing the final result
	 * 
	 * @param size
	 *            Size of the biggest super group
	 * @param winner
	 *            List of names in the biggest super group
	 */
	private static void printWinner(int size, String winner)
	{
		System.out.println(size);
		// Removing not needed chars
		System.out.println(winner.toString().replaceAll("[\\[\\],]", ""));
	}

	/**
	 * Represents the group of people, which are input data to the program.
	 */
	private class FriendsList
	{
		// List of people names, equivalent of the vertex in the graph
		private Set<String> names = new HashSet<String>();
		// List of pair of people, which knows each other
		private Set<Pair> pairs = new HashSet<Pair>();
		// Map for storing the number of friends for each person
		private Map<String, Integer> friendsCount = new HashMap<String, Integer>();

		/**
		 * Check if people with given names know each other. If they are in the same pair.
		 * 
		 * @param first
		 *            Name of the first person in the potential pair.
		 * @param second
		 *            Name of the second person in the potential pair.
		 * @return
		 */
		public boolean isPair(String first, String second)
		{
			return pairs.contains(new Pair(first, second));
		}

		/**
		 * Adds name to the name list
		 * 
		 * @param name
		 *            Name of the person.
		 */
		public void addName(String name)
		{
			names.add(name);
		}

		/**
		 * Adds pair to the pair list and updates the friendsCount with each name.
		 * 
		 * @param first
		 *            Name of the first person in the pair.
		 * @param second
		 *            Name of the second person in the pair.
		 * 
		 * @return true if given pair can be added, false otherwise
		 */
		public boolean addPair(String first, String second)
		{
			boolean canAdd = pairs.add(new Pair(first, second));
			if (canAdd)
			{
				updateFriendsCount(first);
				updateFriendsCount(second);
			}
			return canAdd;
		}

		/**
		 * Updated the count of friends for given person. If person was not yet in the map it adds
		 * it and set the friends count to 1.
		 * 
		 * @param name
		 *            Name of the given person.
		 */
		public void updateFriendsCount(String name)
		{
			if (friendsCount.get(name) == null)
			{
				friendsCount.put(name, 1);
			}
			else
			{
				friendsCount.put(name, friendsCount.get(name) + 1);
			}
		}

		/**
		 * Getter for names set.
		 * 
		 * @return Set of unique names of people.
		 */
		public Set<String> getNames()
		{
			return names;
		}

		/**
		 * Getter for map of the people's friends count.
		 * 
		 * @return Map with key as a name of the person and value as a number of its friends.
		 */
		public Map<String, Integer> getFriendsCount()
		{
			return friendsCount;
		}
	}

	/**
	 * Represents a pair of people, which know each other. Equivalent of the edge in the graph.
	 */
	private class Pair
	{
		public final String first;
		public final String second;

		public Pair(String first, String second)
		{
			this.first = first;
			this.second = second;
		}

		// Important for sorting by name
		@Override
		public int hashCode()
		{
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			int firstString = prime * result + ((first == null) ? 0 : first.hashCode());
			int secondString = prime * result + ((second == null) ? 0 : second.hashCode());
			// custom hash code, to do not care about set order
			result = firstString + secondString;
			return result;
		}

		// Important for sorting by name
		@Override
		public boolean equals(Object obj)
		{
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Pair other = (Pair) obj;
			return (first.equals(other.first) || first.equals(other.second))
			        && (second.equals(other.second) || second.equals(other.first));
		}

		private Main getOuterType()
		{
			return Main.this;
		}
	}

	/**
	 * Method used during development phase to speed up process.
	 * 
	 * @return Mock FriendsList object for testing the code.
	 */
	private static FriendsList getMockFriendList()
	{
		Main outer = new Main();
		FriendsList friendsList = outer.new FriendsList();
		friendsList.addName("Ala");
		friendsList.addName("Ola");
		friendsList.addName("Mirek");
		friendsList.addName("Janek");
		friendsList.addName("Kasia");
		friendsList.addName("Ela");
		friendsList.addName("Zosia");
		friendsList.addPair("Ala", "Ola");
		friendsList.addPair("Ola", "Mirek");
		friendsList.addPair("Janek", "Kasia");
		friendsList.addPair("Kasia", "Ala");
		friendsList.addPair("Ala", "Mirek");
		friendsList.addPair("Janek", "Ala");
		friendsList.addPair("Mirek", "Ela");
		friendsList.addPair("Zosia", "Janek");
		return friendsList;
	}
}